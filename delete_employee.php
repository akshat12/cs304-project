<!-- delete_employee.php -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="custom/images/databass.png">

    <title>Databass Library</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="custom/css/signin.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
    		$("#show_modal_btn").on("click",function(){
    			$("#myModal").modal();
    		}); 
    	});
    </script>

	<style type="text/css">
		.modal-dialog {
		  width: auto;
		  height: auto;
		  padding: 0;
		}

		.modal-content {
		  height: auto;
		  border-radius: 0;
		}
    </style>

    <!-- PHP Files for reporting errors and sql functions -->
    <?php 
    	include "report_errors.php";
	  	include "php/sql-functions.php"; 
  	?>
  </head>
  
  <!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Showing Database Table</h4>
	      </div>
	      <div class="modal-body">
	        <table class="table" id="db-table">
	        </table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
 
<body>
	<div class="container">
		<button type="button" class="btn btn-default" onclick="history.go(-1);"><span class="glyphicon glyphicon-chevron-left"></span> Back to Home </button>

		<hr>

        <div class="jumbotron text-center" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px; background-color: #DDDDDD;">
        	<!-- PHP Script for deleting employees-->
			 <?php 
			  		$employee_id = $_POST['employee-delete-query-text'];

			  		function delete_info($id, $conn){
						$query = "DELETE FROM Librarians L WHERE L.employee_id=";
						$stmt = $query . $id;
						$s = oci_parse($conn, $stmt);
						$r = oci_execute($s);

						return $r;
					}

			  		if($db_conn){
						$result = executePlainSQL("SELECT COUNT(DISTINCT L.employee_id)  AS NUMENTRIES FROM Librarians L WHERE L.employee_id = ". $employee_id);
						oci_fetch_all($result, $row);

						$num_entries = intval($row["NUMENTRIES"][0]);
						$employee_exists = ($num_entries == 1)? true : false ; 

						if($employee_exists){
							$bool_result = delete_info($employee_id, $db_conn);
				        	echo "<h2> Deleting Information For Employee ID : {$employee_id} </h2> <hr>";

							if ($bool_result) {
								echo "<h3 style='color:#4cae4c;'>Successfully Deleted The Employee With ID : {$employee_id} </h3>";
							} else {
								echo "<h3 style='color:#d43f3a;'>There was some problem in deleting the information :(</h3>";
							}
			  			}
			  			else{ //The given employee does not exist
			  				echo "<h3 style='color:#428bca;'> The Employee with ID : {$employee_id} does not exist</h3>";
			  			}
						$tableToDisplay = getTable("LIBRARIANS");
						echo "<script>$('#db-table','.modal-body').append('{$tableToDisplay}')</script>";
						echo "<button class='btn btn-primary btn-lg' id='show_modal_btn' data-toggle='modal' data-target='#myModal'>Show Database Table For Librarians</button>";

			  			logoff_oci();
			  		}
			  		else{
			  			echo "<script>alert('Error Connecting to Database :(. Please Try Again Later.')</script>";
			  		}
			  ?>
        </div>


	</div> <!-- End of container div-->

</body>
</html>